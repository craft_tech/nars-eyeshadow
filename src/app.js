import 'normalize.css'
import './styles/main.css'

import './scripts/rem.js'
import './scripts/main.js'

// import Vue from 'vue'
import Vue from 'vue/dist/vue.esm.js'
import App from './app.vue'

import store from './scripts/store'

import router from './scripts/router'
Vue.config.productionTip = false

import axios from 'axios'
import VueAxios from 'vue-axios'
Vue.use(VueAxios, axios);

import qs from 'qs'

import md5 from 'js-md5';
Vue.prototype.$md5 = md5;


/////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////

import globalVariable from './scripts/global_variable.js'
import Utils from './utils/utils.js'

Vue.prototype._GLOBAL = globalVariable;
Vue.prototype.Utils = Utils;

Vue.prototype.Utils.$MethodFrequencyCheck.init()

axios.defaults.baseURL = Vue.prototype._GLOBAL.data.apiDomain;
Vue.prototype._GLOBAL.qs = qs;

if (router) {
    // Vue.prototype.Utils.$router = new Vue.prototype.Utils.changeRouter(router)
    Vue.prototype.Utils.$router.init(router)
}

/////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * 授权
 * 放到html里
 */
// import wxRedirect from './scripts/wx-redirect.js'

var _openid = Vue.prototype.Utils.$url.getQueryParameter('info');
// console.log('openid:', _openid)

if (_openid) {
    Vue.prototype._GLOBAL.userInfo.openId = _openid;
} else {
    // if (Vue.prototype._GLOBAL.device.wechat) {
    //     location.replace(Vue.prototype._GLOBAL.data.wxOauth + window.location.href.split('#')[0]);
    // }
}

var _source = Vue.prototype.Utils.$url.getQueryParameter('utm_source');
Vue.prototype._GLOBAL.userInfo.source = _source;


var originalHeight = document.documentElement.clientHeight || document.body.clientHeight;
window.onresize = function () {
    var resizeHeight = document.documentElement.clientHeight || document.body.clientHeight;
    if (resizeHeight * 1 < originalHeight * 1) { //resizeHeight<originalHeight被挤压
        document.body.classList.add('isUseInput')
    } else {
        document.body.classList.remove('isUseInput')
    }

    // if (Vue.prototype._GLOBAL.device.wechat && Vue.prototype._GLOBAL.device.ios && !_openid) {
    //     document.body.style.height = (window.innerHeight - 45) + 'px';
    // }
};


window.alert = function (name) {
    var iframe = document.createElement("IFRAME");
    iframe.style.display = "none";
    iframe.setAttribute("src", 'data:text/plain,');
    document.documentElement.appendChild(iframe);
    window.frames[0].window.alert(name);
    iframe.parentNode.removeChild(iframe);
};

/**
 * 阻止默认的处理方式(阻止下拉滑动的效果)
 */
document.body.addEventListener('touchmove', function (e) {
    if (Vue.prototype._GLOBAL.data.preventDefault) {
        e.preventDefault();
    }
}, {
    // passive: true
    passive: Vue.prototype._GLOBAL.data.passive
})

// function pushHistory() {
//     var state = {
//         title: "",
//         url: "#"
//     };
//     window.history.pushState(state, "", "");
//     // document.body.style.height = Vue.prototype.Utils.$Dom.getStyle(document.body, 'height')
//     document.body.style.height = window.innerHeight + 'px';
// }
// pushHistory();
// window.addEventListener("popstate", function(e) {
//     WeixinJSBridge.invoke('closeWindow', {}, function(res) {});
// }, false);
// alert(window.innerHeight)
// if (Vue.prototype._GLOBAL.device.wechat && Vue.prototype._GLOBAL.device.ios) {
//     document.body.style.height = (window.innerHeight - 45) + 'px';
// }




import 'swiper/dist/css/swiper.css'
import VueAwesomeSwiper from 'vue-awesome-swiper'
Vue.use(VueAwesomeSwiper)





/**
 * 提示页跳转
 */
const webpackConfig = require('../config/webpack.config.js');
if (!Vue.prototype._GLOBAL.isDevtools && !Vue.prototype._GLOBAL.device.mobile) {
    // location.replace(webpackConfig.publicPath_prod + 'wx.html');
}


// if (!Vue.prototype._GLOBAL.isDevtools && !Vue.prototype._GLOBAL.device.wechat) {
//     if (!Vue.prototype._GLOBAL.device.weibo) {
//         //手机浏览器app
//         if (Vue.prototype._GLOBAL.device.android && !Vue.prototype._GLOBAL.device.androidChrome) { //不支持的安卓浏览器
//             location.replace('./wx.html');
//         }
//         if (Vue.prototype._GLOBAL.device.ios) { //不支持的ios浏览器
//             // location.replace('./wx.html');
//         }
//     } else {

//     }
// }

(function () {
    if (typeof WeixinJSBridge == "object" && typeof WeixinJSBridge.invoke == "function") {
        handleFontSize();
    } else {
        if (document.addEventListener) {
            document.addEventListener("WeixinJSBridgeReady", handleFontSize, false);
        } else if (document.attachEvent) {
            document.attachEvent("WeixinJSBridgeReady", handleFontSize);
            document.attachEvent("onWeixinJSBridgeReady", handleFontSize);
        }
    }

    function handleFontSize() {
        // 设置网页字体为默认大小
        WeixinJSBridge.invoke('setFontSizeCallback', {
            'fontSize': 0
        });
        // 重写设置网页字体大小的事件
        WeixinJSBridge.on('menu:setfont', function () {
            WeixinJSBridge.invoke('setFontSizeCallback', {
                'fontSize': 0
            });
        });
    }
})();
/**
 * vConsole
 */
// import VConsole from 'vconsole/dist/vconsole.min.js'
// if (Vue.prototype._GLOBAL.device.wechat && Vue.prototype._GLOBAL.isDevtools) {
//     // if (Vue.prototype._GLOBAL.device.mobile && Vue.prototype._GLOBAL.isDevtools) {
//     window.vConsole = new VConsole();
//     console.log(vConsole)
// }


/**
 * eruda Console
 */
// (function() {
//     var script = document.createElement('script');
//     script.src = "//cdn.jsdelivr.net/npm/eruda";
//     document.body.appendChild(script);
//     script.onload = function() {
//         eruda.init()
//     }
// })();

/////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////

if (Vue.prototype._GLOBAL.isDevtools) {
    window.app = Vue.prototype._GLOBAL;
}

Vue.prototype._GLOBAL.eventBus = new Vue();

var app = new Vue({
    el: '#app',
    router,
    store,
    template: '<app/>', //template参数 > el 外部HTML
    components: {
        app: App
    },
    created() {},
    mounted() {
        // 禁用vconsole
        if (!Vue.prototype._GLOBAL.isDevtools) {
            let check_vcons_st = ''
            let check_vcons = () => {
                clearTimeout(check_vcons_st)
                check_vcons_st = setTimeout(() => {
                    const div_v = document.getElementById('__vconsole')
                    if (div_v) {
                        clearTimeout(check_vcons_st)
                        div_v.style.fontSize = 0;
                    } else {
                        check_vcons()
                    }
                }, 100)
            }
            check_vcons()
        }
    }
});