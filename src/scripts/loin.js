document.addEventListener("DOMContentLoaded", function(event) {

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var parser = new DOMParser();
            var doc = parser.parseFromString(this.responseText, "text/html");

            function loadHead() {
                var range, documentFragment, node, firstChild;
                var range = document.createRange();
                var node = document.head;
                range.selectNode(node);
                var documentFragment = range.createContextualFragment(doc.head.innerHTML);
                var firstChild = node.firstChild;
                while (firstChild) {
                    node.removeChild(firstChild);
                    firstChild = node.firstChild;
                }
                node.appendChild(documentFragment);
            }
            loadHead();

            function loadBody() {
                var range, documentFragment, node, firstChild;
                var range = document.createRange();
                var node = document.body;
                range.selectNode(node);
                var documentFragment = range.createContextualFragment(doc.body
                    .innerHTML);
                var firstChild = node.firstChild;
                while (firstChild) {
                    node.removeChild(firstChild);
                    firstChild = node.firstChild;
                }
                node.appendChild(documentFragment);
            }

            var cssMain = document.head.querySelectorAll('link');
            var cssLength = cssMain.length;
            if (cssLength > 0) {
                for (var i = 0; i < cssMain.length; i++) {
                    cssMain[i].addEventListener('load', function() {
                        cssLength--;
                        if (cssLength == 0) {
                            loadBody();
                        }
                    });
                }
            } else {
                loadBody();
            }
        }
    };




    function padDigits(number, digits) {
        return Array(Math.max(digits - String(number).length + 1, 0)).join(0) + number;
    }

    var datetime = new Date();

    var year = datetime.getFullYear();
    var month = datetime.getMonth() + 1;
    var date = datetime.getDate();
    var hours = datetime.getHours();
    var minutes = datetime.getMinutes();
    minutes = minutes - minutes % 2;

    var timezoneOffset = -datetime.getTimezoneOffset();
    var timezoneOffsetString = "_GMT" + (timezoneOffset >= 0 ? "+" : "-") + padDigits(Math.floor(
        timezoneOffset / 60), 2) + padDigits(timezoneOffset % 60, 2);

    var dateString = year + "-" + padDigits(month, 2) + "-" + padDigits(date, 2) + "_" + padDigits(
        hours, 2) + ":" + padDigits(minutes, 2) + timezoneOffsetString;

    xhttp.open("GET", "index .html?" + datetime.getTime(), true);
    xhttp.send();

});