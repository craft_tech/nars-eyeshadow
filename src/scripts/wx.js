function getQueryParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)'),
        results = regex.exec(location.search);
    return results == null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}
if (navigator.userAgent.toLowerCase().indexOf("micromessenger") != -1) {
    if (!getQueryParameter('token')) {
        // location.replace("https://campaign.lancome.com.cn/recycle/Wechat/Oauth2?siteurl=" + window.location.href.split('#')[0]);
    }
}