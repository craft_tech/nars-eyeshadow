var wxr_domain_staging = 'campaign.ipsa.com.cn';
var wxr_domain_production = 'campaign.ipsa.com.cn';



function wxr_getParameterByName(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)'),
        results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}

function wxr_setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = 'expires=' + d.toUTCString();
    document.cookie = cname + '=' + cvalue + '; ' + expires;
}

function wxr_getCookie(cname) {
    var name = cname + '=';
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return '';
}


function wxr_removeParameter(url, parameter) {

    var urlparts = url.split('?');

    if (urlparts.length >= 2) {
        var urlBase = urlparts.shift();
        var queryString = urlparts.join('?');

        var prefix = encodeURIComponent(parameter) + '=';
        var pars = queryString.split(/[&;]/g);
        for (var i = pars.length; i-- > 0;)
            if (pars[i].lastIndexOf(prefix, 0) !== -1)
                pars.splice(i, 1);
        url = urlBase + '?' + pars.join('&');

    }
    return url;
}




var wxr_openid = '';
var wxr_nickname = '';
var wxr_headimgurl = '';



if (wxr_getParameterByName('openid')) {
    wxr_openid = wxr_getParameterByName('openid');
}

if (wxr_getParameterByName('nickname')) {
    wxr_nickname = wxr_getParameterByName('nickname');
}

if (wxr_getParameterByName('headimgurl')) {
    wxr_headimgurl = wxr_getParameterByName('headimgurl');
}

if (navigator.userAgent.toLowerCase().indexOf("micromessenger") != -1) {

    if ((window.location.host.indexOf(wxr_domain_staging) == 0 || window.location.host.indexOf(wxr_domain_production) == 0)) {
        console.log('0');

        if (wxr_openid == '' && wxr_nickname == '') {

            // var link = encodeURIComponent('https://' + window.location.host + '/service/otheroauth?backurl=' + encodeURIComponent(window.location.href));

            // var redirect = encodeURIComponent("http://wx-proxy.lorealparis.com.cn/sns/oauth2/access_token/lrl/?link=" + link);

            // document.location = "https://px02331.wetalk.im/connect/oauth2/authorize2?appid=wx8e0f1950ce4817cd&redirect_uri=" + redirect + "&response_type=code&scope=snsapi_userinfo&state=#wechat_redirect";

            location.href = 'https://campaign.narscosmetics.com.cn/radiancerepowedchallengevote/Service/Oauth?backurl=' + encodeURIComponent(window.location.href.split('#')[0]);
            console.log(1)

        } else if (wxr_openid != '' && wxr_nickname != '') {

            wxr_setCookie('nickname', encodeURIComponent(wxr_nickname), 1 / 48);
            wxr_setCookie('headimgurl', encodeURIComponent(wxr_headimgurl), 1 / 48);

            var url = document.location.href;
            url = wxr_removeParameter(url, 'nickname');
            url = wxr_removeParameter(url, 'headimgurl');

            document.location = url;

        } else {

            wxr_nickname = decodeURIComponent(wxr_getCookie('nickname'));
            wxr_headimgurl = decodeURIComponent(wxr_getCookie('headimgurl'));

            wxr_setCookie('nickname', '', 1 / 48);
            wxr_setCookie('headimgurl', '', 1 / 48);

        }





    }

}